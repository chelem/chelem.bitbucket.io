(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/@angular/core/fesm5 lazy recursive":
/*!****************************************************************!*\
  !*** ./node_modules/@angular/core/fesm5 lazy namespace object ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./node_modules/@angular/core/fesm5 lazy recursive";

/***/ }),

/***/ "./src/app/annotation/annotation.component.html":
/*!******************************************************!*\
  !*** ./src/app/annotation/annotation.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"HIT\">\r\n  <app-categories [categories]=\"HIT.categories\" (category-changed)=\"onCategoryChanged($event)\"></app-categories>\r\n  <app-passage [passage]=\"passage\" [selected-category]=\"selectedCategory\"></app-passage>\r\n  <!--\r\n    app-submit [passage-result]=\"getPassageResult()\" [allow-submit]=\"isSubmitAllowed()\"\r\n  -->\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/annotation/annotation.component.scss":
/*!******************************************************!*\
  !*** ./src/app/annotation/annotation.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Fubm90YXRpb24vYW5ub3RhdGlvbi5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/annotation/annotation.component.ts":
/*!****************************************************!*\
  !*** ./src/app/annotation/annotation.component.ts ***!
  \****************************************************/
/*! exports provided: AnnotationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnnotationComponent", function() { return AnnotationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_hit_info_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/hit-info.service */ "./src/app/services/hit-info.service.ts");
/* harmony import */ var _models_annotations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/annotations */ "./src/app/models/annotations.ts");




var AnnotationComponent = /** @class */ (function () {
    function AnnotationComponent(hitInfoService) {
        this.hitInfoService = hitInfoService;
    }
    AnnotationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.hitInfoService.getHitInfo().subscribe(function (hit) {
            _this.HIT = hit;
            _this.passage = new _models_annotations__WEBPACK_IMPORTED_MODULE_3__["Passage"](hit.passage);
        });
    };
    AnnotationComponent.prototype.onCategoryChanged = function (category) {
        this.selectedCategory = category;
    };
    AnnotationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-annotation',
            template: __webpack_require__(/*! ./annotation.component.html */ "./src/app/annotation/annotation.component.html"),
            styles: [__webpack_require__(/*! ./annotation.component.scss */ "./src/app/annotation/annotation.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_hit_info_service__WEBPACK_IMPORTED_MODULE_2__["HitInfoService"]])
    ], AnnotationComponent);
    return AnnotationComponent;
}());



/***/ }),

/***/ "./src/app/annotation/categories/categories.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/annotation/categories/categories.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col\">\r\n    <div  class=\"btn-group btn-group-toggle\" ngbRadioGroup role=\"group\">\r\n      <button *ngFor=\"let cat of categories; let i = index\" \r\n              class=\"btn\" \r\n              [ngStyle]=\"{'background-color': cat.color}\" \r\n              (click)=\"onCategoryChanged(cat)\">\r\n        {{cat.name}}\r\n      </button>\r\n      <button class=\"btn\" id=\"untag-button\" (click)=\"onCategoryChanged(null)\">Untag</button>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-4\">\r\n    <span id=\"selected-category\" [ngStyle]=\"{'background-color': selectedColor }\">\r\n      {{ selectedTitle }}\r\n    </span>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/annotation/categories/categories.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/annotation/categories/categories.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#selected-category {\n  border: 1px solid; }\n\nbutton {\n  border: 1px solid; }\n\n#untag-button {\n  background-color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYW5ub3RhdGlvbi9jYXRlZ29yaWVzL0M6XFxVc2Vyc1xcaXRheVxcc291cmNlc1xcRGFmbmFTaGFoYWZcXE1UdXJrQW5ub3RhdG9yXFxmcm9udGVuZC9zcmNcXGFwcFxcYW5ub3RhdGlvblxcY2F0ZWdvcmllc1xcY2F0ZWdvcmllcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFpQixFQUNwQjs7QUFFRDtFQUNJLGtCQUFpQixFQUNwQjs7QUFFRDtFQUNJLHdCQUF1QixFQUMxQiIsImZpbGUiOiJzcmMvYXBwL2Fubm90YXRpb24vY2F0ZWdvcmllcy9jYXRlZ29yaWVzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI3NlbGVjdGVkLWNhdGVnb3J5IHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG59XHJcblxyXG5idXR0b24ge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbn1cclxuXHJcbiN1bnRhZy1idXR0b24ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/annotation/categories/categories.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/annotation/categories/categories.component.ts ***!
  \***************************************************************/
/*! exports provided: CategoriesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesComponent", function() { return CategoriesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CategoriesComponent = /** @class */ (function () {
    function CategoriesComponent() {
        this.categoryChanged = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.selectedCategory = null;
    }
    Object.defineProperty(CategoriesComponent.prototype, "selectedColor", {
        get: function () {
            return this.selectedCategory ? this.selectedCategory.color : 'white';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CategoriesComponent.prototype, "selectedTitle", {
        get: function () {
            return this.selectedCategory ? this.selectedCategory.name : 'Untag';
        },
        enumerable: true,
        configurable: true
    });
    CategoriesComponent.prototype.onCategoryChanged = function (cat) {
        this.selectedCategory = cat;
        this.categoryChanged.emit(this.selectedCategory);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])('category-changed'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CategoriesComponent.prototype, "categoryChanged", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], CategoriesComponent.prototype, "categories", void 0);
    CategoriesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-categories',
            template: __webpack_require__(/*! ./categories.component.html */ "./src/app/annotation/categories/categories.component.html"),
            styles: [__webpack_require__(/*! ./categories.component.scss */ "./src/app/annotation/categories/categories.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CategoriesComponent);
    return CategoriesComponent;
}());



/***/ }),

/***/ "./src/app/annotation/passage/passage.component.html":
/*!***********************************************************!*\
  !*** ./src/app/annotation/passage/passage.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"section\" *ngFor=\"let section of passage.sections\" [className]=\"getSectionClass(section)\">\r\n  <ng-container *ngFor=\"let part of section.parts\">\r\n    <span *ngIf=\"isTaggable(part)\" class=\"taggable\" [ngStyle]=\"getTaggableStyle(part)\" (click)=\"onPartClick(part)\">{{ part.text }}</span>\r\n    <span *ngIf=\"!isTaggable(part)\" class=\"untaggable\">{{ part.text }}</span>\r\n  </ng-container>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/annotation/passage/passage.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/annotation/passage/passage.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "span.taggable {\n  border: 1px solid;\n  cursor: pointer; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYW5ub3RhdGlvbi9wYXNzYWdlL0M6XFxVc2Vyc1xcaXRheVxcc291cmNlc1xcRGFmbmFTaGFoYWZcXE1UdXJrQW5ub3RhdG9yXFxmcm9udGVuZC9zcmNcXGFwcFxcYW5ub3RhdGlvblxccGFzc2FnZVxccGFzc2FnZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFpQjtFQUNqQixnQkFBZSxFQUNsQiIsImZpbGUiOiJzcmMvYXBwL2Fubm90YXRpb24vcGFzc2FnZS9wYXNzYWdlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsic3Bhbi50YWdnYWJsZSB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/annotation/passage/passage.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/annotation/passage/passage.component.ts ***!
  \*********************************************************/
/*! exports provided: PassageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PassageComponent", function() { return PassageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_models_annotations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/models/annotations */ "./src/app/models/annotations.ts");



var PassageComponent = /** @class */ (function () {
    function PassageComponent() {
    }
    PassageComponent.prototype.ngOnInit = function () {
    };
    PassageComponent.prototype.isTaggable = function (part) {
        return part instanceof src_app_models_annotations__WEBPACK_IMPORTED_MODULE_2__["TaggablePart"];
    };
    PassageComponent.prototype.onPartClick = function (part) {
        console.log('Setting category of ', part.text, ' to ', this.selectedCategory);
        part.category = this.selectedCategory;
    };
    PassageComponent.prototype.getTaggableStyle = function (part) {
        var color = part.category ? part.category.color : 'white';
        return { 'background-color': color };
    };
    PassageComponent.prototype.getSectionClass = function (section) {
        return "section-" + section.type;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", src_app_models_annotations__WEBPACK_IMPORTED_MODULE_2__["Passage"])
    ], PassageComponent.prototype, "passage", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('selected-category'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], PassageComponent.prototype, "selectedCategory", void 0);
    PassageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-passage',
            template: __webpack_require__(/*! ./passage.component.html */ "./src/app/annotation/passage/passage.component.html"),
            styles: [__webpack_require__(/*! ./passage.component.scss */ "./src/app/annotation/passage/passage.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PassageComponent);
    return PassageComponent;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <div class=\"row\">\r\n    <app-explanation></app-explanation>\r\n  </div>\r\n  <div class=\"row\">\r\n    <app-annotation></app-annotation>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'MTurk Annotations';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _explanation_explanation_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./explanation/explanation.component */ "./src/app/explanation/explanation.component.ts");
/* harmony import */ var _annotation_annotation_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./annotation/annotation.component */ "./src/app/annotation/annotation.component.ts");
/* harmony import */ var _annotation_categories_categories_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./annotation/categories/categories.component */ "./src/app/annotation/categories/categories.component.ts");
/* harmony import */ var _annotation_passage_passage_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./annotation/passage/passage.component */ "./src/app/annotation/passage/passage.component.ts");










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _explanation_explanation_component__WEBPACK_IMPORTED_MODULE_6__["ExplanationComponent"],
                _annotation_annotation_component__WEBPACK_IMPORTED_MODULE_7__["AnnotationComponent"],
                _annotation_categories_categories_component__WEBPACK_IMPORTED_MODULE_8__["CategoriesComponent"],
                _annotation_passage_passage_component__WEBPACK_IMPORTED_MODULE_9__["PassageComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModule"],
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/explanation/explanation.component.html":
/*!********************************************************!*\
  !*** ./src/app/explanation/explanation.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <label>{{ explanation }}</label>\r\n</div>"

/***/ }),

/***/ "./src/app/explanation/explanation.component.scss":
/*!********************************************************!*\
  !*** ./src/app/explanation/explanation.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2V4cGxhbmF0aW9uL2V4cGxhbmF0aW9uLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/explanation/explanation.component.ts":
/*!******************************************************!*\
  !*** ./src/app/explanation/explanation.component.ts ***!
  \******************************************************/
/*! exports provided: ExplanationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExplanationComponent", function() { return ExplanationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ExplanationComponent = /** @class */ (function () {
    function ExplanationComponent() {
        this.getExplanation();
    }
    ExplanationComponent.prototype.ngOnInit = function () {
    };
    ExplanationComponent.prototype.getExplanation = function () {
        this.explanation = 'dsfh hdgfjher gkgh rtgnkfg ehtie  ojrt ejrtg jiy  preoiupwepr eirtyioi ujti45y6 vp54oyop4 otuy tjyopet jyo e';
    };
    ExplanationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-explanation',
            template: __webpack_require__(/*! ./explanation.component.html */ "./src/app/explanation/explanation.component.html"),
            styles: [__webpack_require__(/*! ./explanation.component.scss */ "./src/app/explanation/explanation.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ExplanationComponent);
    return ExplanationComponent;
}());



/***/ }),

/***/ "./src/app/models/annotations.ts":
/*!***************************************!*\
  !*** ./src/app/models/annotations.ts ***!
  \***************************************/
/*! exports provided: PassagePart, TaggablePart, Section, Passage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PassagePart", function() { return PassagePart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaggablePart", function() { return TaggablePart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Section", function() { return Section; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Passage", function() { return Passage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var PassagePart = /** @class */ (function () {
    function PassagePart(text) {
        this.text = text;
    }
    return PassagePart;
}());

var TaggablePart = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TaggablePart, _super);
    function TaggablePart() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return TaggablePart;
}(PassagePart));

var Section = /** @class */ (function () {
    function Section() {
        this.parts = [];
    }
    return Section;
}());

var Passage = /** @class */ (function () {
    function Passage(xmlPassage) {
        this.sections = [];
        var parser = new DOMParser();
        var xml = parser.parseFromString(xmlPassage, 'text/xml');
        if (xml.documentElement.tagName !== 'text') {
            console.error('Expected root element to be <text>', xmlPassage);
            return;
        }
        var sectionTags = xml.getElementsByTagName('section');
        for (var i = 0; i < sectionTags.length; i++) {
            this.appendSection(sectionTags.item(i));
        }
    }
    Passage.prototype.appendSection = function (node) {
        var section = new Section();
        section.type = node.getAttribute('type') || 'default';
        for (var i = 0; i < node.childNodes.length; i++) {
            var part = void 0;
            var child = node.childNodes.item(i);
            switch (child.nodeName) {
                case 'taggable':
                    part = new TaggablePart(child.textContent);
                    break;
                case '#text':
                    part = new PassagePart(child.textContent);
                    break;
                default:
                    console.error('Unexpected section child of type ', child.nodeName);
            }
            section.parts.push(part);
        }
        this.sections.push(section);
    };
    Passage.prototype.getSubmitResult = function () {
        // Return 
    };
    return Passage;
}());



/***/ }),

/***/ "./src/app/services/hit-info.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/hit-info.service.ts ***!
  \**********************************************/
/*! exports provided: HitInfoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HitInfoService", function() { return HitInfoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _parameters_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./parameters.service */ "./src/app/services/parameters.service.ts");






var HitInfoService = /** @class */ (function () {
    function HitInfoService(http, parameters) {
        this.http = http;
        this.parameters = parameters;
        this._hitInfo = undefined;
        this._colors = ['red', 'yellow', 'blue', 'brown', 'pink', 'green', 'orange'];
    }
    HitInfoService.prototype.getHitInfo = function () {
        var _this = this;
        if (this._hitInfo) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(this._hitInfo);
        }
        var parameters = this.parameters.getParameters();
        return this.http.get(parameters.textUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) {
            _this._hitInfo = _this.createHitInfo(response, parameters);
            return _this._hitInfo;
        }));
    };
    HitInfoService.prototype.createHitInfo = function (response, parameters) {
        var _this = this;
        var categories = response.categories.map(function (name, index) { return ({ name: name, color: _this._colors[index] }); });
        var passage = response.passages[parameters.textIndex];
        var hit = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ passage: passage,
            categories: categories }, parameters);
        return hit;
    };
    HitInfoService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _parameters_service__WEBPACK_IMPORTED_MODULE_5__["ParametersService"]])
    ], HitInfoService);
    return HitInfoService;
}());



/***/ }),

/***/ "./src/app/services/parameters.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/parameters.service.ts ***!
  \************************************************/
/*! exports provided: ParametersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParametersService", function() { return ParametersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ParametersService = /** @class */ (function () {
    function ParametersService() {
    }
    ParametersService.prototype.getUrlVars = function () {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            vars[key] = value;
            return key;
        });
        return vars;
    };
    ParametersService.prototype.getParameters = function () {
        var params = this.getUrlVars();
        return {
            hitId: params['hitId'],
            assignmentId: params['assignmentId'],
            workerId: params['workerId'],
            textUrl: params['textUrl'] || '/assets/projects/dev.json',
            textIndex: params['textIndex'],
        };
    };
    ParametersService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ParametersService);
    return ParametersService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\itay\sources\DafnaShahaf\MTurkAnnotator\frontend\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map